#ifndef __linux
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <crtdbg.h> 
#include "jni.h"
#include "string.h"
#include "direct.h"
#include <time.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <math.h>
#include <map>
#include <string>
#include <vector>
#include <time.h>
#endif

#ifdef __linux
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <cerrno>
#include <cstring>
#include <climits>
#include <ctime>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <vector>


#define closesocket close
#define TRUE 1
#define FALSE 0
#define SOCKET int
#define _stricmp strcasecmp
#define WSADATA int

typedef const char* LPCSTR;
typedef char * PSTR, * LPSTR;
typedef unsigned long long int DWORD, *PWORD, *LPWORD;
typedef int BOOL;

int INVALID_SOCKET = -1;
int SOCKET_ERROR = -1;
void WSACleanup();
void LocalFree(char * msg);
int WSAGetLastError();
void WSASetLastError(int errno);
LPSTR GetCommandLineA();
DWORD GetCurrentProcessId();




DWORD GetCurrentProcessId() {
  return (DWORD) getpid();
}


LPSTR GetCommandLineA() {
  char * path = NULL;
  char * msg = NULL;
  char * cmdline = new char[_POSIX_ARG_MAX];
  FILE * file;
  size_t read = 0;
  size_t i = 0;

  path = new char[1024];

  sprintf(path, "/proc/%ld/cmdline", (long) getpid());
  file = fopen(path, "r");

  if (file == NULL) {
    msg = new char[1024];
    sprintf(msg, "Could not open file \"%s\"", path);
    perror(msg);
    goto ERROR_EXIT;

  }

  read = fread(cmdline, sizeof(char), _POSIX_ARG_MAX, file);
  i = 0;
  while(i++ < read) {
    if (cmdline[i] == 0) {
      cmdline[i] = ' ';
    }
  }
  cmdline[read-1] = (char) NULL;

ERROR_EXIT:
  if (path)
    delete path;
  if (msg)
    delete msg;
  if (file)
    fclose(file);

  return cmdline;
}

void WSACleanup() {}
void LocalFree(char * msg) {}
void WSASetLastError(int errno) {}

int WSAGetLastError() {
  return errno;
}


#endif


char * WSAGetLastErrorMsg();
void read_parameters();
void open_log(char* file);
void close_log();

char * timestamp();
void copy_error_message(char * output, int outputSize, char * msg);
static char version[] = "1.0";

using namespace std;

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define LOG_TIME_FORMAT "%b %d %H:%M:%S"
#define LOG_INFO(format, ...) logfh && (fprintf(logfh, "%s INFO: "  format, timestamp(), ##__VA_ARGS__))
#define LOG_DEBUG(format, ...) logfh && *sock_debug_value && (fprintf(logfh, "%s DEBUG: "  format, timestamp(), ##__VA_ARGS__))
#define LOG_WARNING(format, ...) logfh && (fprintf(logfh, "%s WARNING: "  format, timestamp(), ##__VA_ARGS__))
#define LOG_ERROR(format, ...) logfh && (fprintf(logfh, "%s ERROR: "  format, timestamp(), ##__VA_ARGS__))

#define DEFAULT_CHUNK_LIFETIME 10


#define BENCHMARK_SETUP ;
#define BENCHMARK_BEGIN ;
#define BENCHMARK_END(prefix) ;

static FILE * logfh = NULL;
static char time_buffer[100];


char * timestamp() {
  time_t ltime;
  time(&ltime);
  strftime(time_buffer, sizeof(time_buffer), LOG_TIME_FORMAT, localtime(&ltime));
  return time_buffer;
}

void open_log(char* log_file) {
  if (!log_file || (log_file  && (logfh = fopen(log_file, "w+")) == NULL)) {
    char default_log_file[100];
    sprintf(default_log_file,"sock_%llu.log", GetCurrentProcessId());
    logfh = fopen(default_log_file, "w+");
  }

  if (logfh)
    setvbuf(logfh, NULL, _IONBF, 0);
}

void close_log() {
  if (logfh)
    fclose(logfh);
}

#define FILLARRAY(target) (memset(target, 0, sizeof(target)))
#define sock_host "-sock_host"
#define sock_port "-sock_port"
#define sock_debug "-sock_debug"
#define sock_log "-sock_log"
#define sock_chunk_lifetime "-sock_chunk_lifetime"
#define sock_benchmark "-sock_benchmark"
#define FALSE_STR "false"

char sock_host_value[100] = {0};
char sock_port_value[100] = {0};
char sock_debug_value[100] = {0};
char sock_log_value[100] = {0};
unsigned int sock_chunk_lifetime_value= 0;
char sock_benchmark_value[100] = {0};
char compiled_sock_host = '127.0.0.1\n';
char compiled_sock_port = '9090\n';

void read_parameters() {

  LPSTR cmd = GetCommandLineA();

  int i = 0;
  int len = strlen(cmd);
  for (; i < len; i++) {
    char * text = (&cmd[i]);
    if (strncmp(sock_host, text, sizeof(sock_host)-1) == 0) {
      sscanf(text + sizeof(sock_host), "%s", sock_host_value);
    }
    else {
      sock_host_value = compiled_sock_host;
    } 
    if (strncmp(sock_port, text, sizeof(sock_port)-1) == 0) {
      sscanf(text + sizeof(sock_port), "%s", sock_port_value);
    }
    else {
      sock_port_value = compiled_sock_port
    } 
    if (strncmp(sock_debug, text, sizeof(sock_debug)-1) == 0) {
      sscanf(text + sizeof(sock_debug), "%s", sock_debug_value);
    }
    if (strncmp(sock_log, text, sizeof(sock_log)-1) == 0) {
      sscanf(text + sizeof(sock_log), "%s", sock_log_value);
    }
    if (strncmp(sock_chunk_lifetime, text, sizeof(sock_chunk_lifetime)-1) == 0) {
      sscanf(text + sizeof(sock_chunk_lifetime), "%u", &sock_chunk_lifetime_value);
    }
    if (strncmp(sock_benchmark, text, sizeof(sock_benchmark)-1) == 0) {
      sscanf(text + sizeof(sock_benchmark), "%s", sock_benchmark_value);
    }
  }

  open_log(sock_log_value);
  LOG_INFO("cmdline=%s\n", cmd);

  if (*sock_log_value) {
    LOG_INFO("%s=%s\n", sock_log, sock_log_value);
  }

  if (*sock_host_value) {
    LOG_INFO("%s=%s\n", sock_host, sock_host_value);
  }

  if (*sock_port_value) {
    LOG_INFO("%s=%s\n", sock_port, sock_port_value);
  }

  if (*sock_debug_value) {
    LOG_INFO("%s=%s\n", sock_debug, sock_debug_value);
    //cleanup the sock_debug variable, if it's set to false
    if (_stricmp(sock_debug_value, FALSE_STR) == 0) {
      *sock_debug_value = 0;
    }
  }

  if (sock_chunk_lifetime_value > 0) {
     LOG_INFO("%s=%u\n", sock_chunk_lifetime, sock_chunk_lifetime_value);
  }

  if (sock_chunk_lifetime_value == 0) {
      sock_chunk_lifetime_value = DEFAULT_CHUNK_LIFETIME;
      LOG_INFO("%s=%u\n", sock_chunk_lifetime, sock_chunk_lifetime_value);
  }

  if (*sock_benchmark_value) {
    LOG_INFO("%s=%s\n", sock_benchmark, sock_benchmark_value);
    //cleanup the sock_benchmark variable, if it's set to false
    if (_stricmp(sock_benchmark_value, FALSE_STR) == 0) {
      *sock_benchmark_value = 0;
    }
  }

#ifdef __linux
  if (cmd) {
    delete cmd;
  }
#endif
}


/**
 * This class represents a chunk of that that has been received.
 */
class chunk_info {
public:
  string * key;
  char * location;
  time_t created_time;

    chunk_info(char * key, char * location) : key(new string(key)), location(location) {
    time(&created_time);
    };

  ~chunk_info() {
    if (key) {
      delete key;
    }
    if (location) {
      delete location;
    }
  }
};

map<string, chunk_info*> chunk_map;
WSADATA wsaData;
SOCKET client_sock = INVALID_SOCKET;


char * WSAGetLastErrorMsg() {
#ifdef __linux
  return strerror(errno);
#else
  LPSTR err_str;
  FormatMessageA(
    FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
    0,
    WSAGetLastError(),
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
    (LPSTR) &err_str,
    0,
    0);

  return err_str;
#endif
}

void copy_error_message(char * output, int outputSize, char * msg) {
  if (msg == NULL || output == NULL || outputSize <= 0) {
    return;
  }

  int msg_len = strlen(msg);
  int effectiveOutputsize = outputSize - 1 - 2; //-1 for NULL terminator, -2 for begining and ending quotes

  int result_sz = MIN(effectiveOutputsize, msg_len);

  int offset = 0;
  output[offset++] = '"';

  //safely replace all double-quotes with back-ticks
  for (int i = 0; i < result_sz; i++, offset++)
    output[offset] = (msg[i] == '"')? '`': msg[i];

  output[offset++] = '"';

  //fill the rest of the string with NULL
  while (offset < outputSize)
    output[offset++] = (char) NULL;

}


SOCKET * open_socket(char * output, int outputSize) {
  int connectResult;
  DWORD send_timeout;
  DWORD recv_timeout;

  if (client_sock != INVALID_SOCKET)
    return &client_sock;

  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;
  
  char * msg = NULL;

#ifndef __linux
  if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) {
    LOG_ERROR("Socket library startup failed. Error: %d, Message: %s\n", WSAGetLastError(), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }
#endif

  if (!*sock_host_value) {
    LOG_ERROR("Could not open connection, -sock_host=<host> parameter was not specified\n");
    goto ERROR_EXIT;
  }

  if (!*sock_port_value) {
    LOG_ERROR("Could not open connection, -sock_port=<port> parameter was not specified\n");
    goto ERROR_EXIT;
  }


  LOG_INFO("Resolving address for %s:%s\n", sock_host_value, sock_port_value);

  struct addrinfo hints;
  struct addrinfo *result, *ptr;
  memset(&hints, 0, sizeof(struct addrinfo));

  hints.ai_family = AF_UNSPEC;    // Allow IPv4 or IPv6
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;
  hints.ai_protocol = 0;

  if (getaddrinfo(sock_host_value, sock_port_value, &hints, &result)) {
    LOG_ERROR("Opening connection failed. Could not resolve INET address for %s. Error: %d, Message: %s\n", sock_host_value, WSAGetLastError(), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }

  for(ptr = result; ptr != NULL ; ptr=ptr->ai_next) {
    if (ptr->ai_family == AF_UNSPEC) {
      continue;
    }
    else if (ptr->ai_family == AF_INET) {
      struct sockaddr_in * sa = (struct sockaddr_in *) ptr->ai_addr;
      char str[INET_ADDRSTRLEN] = {0};
      inet_ntop(AF_INET, &(sa->sin_addr), str, sizeof(str));
      LOG_INFO("Address resolved to IPv4 \"%s\", and port \"%s\"\n", str, sock_port_value);
      break;
    }
    else if (ptr->ai_family == AF_INET6) {
      struct sockaddr_in6 * sa = (struct sockaddr_in6 *) ptr->ai_addr;
      char str[INET6_ADDRSTRLEN] = {0};
      inet_ntop(AF_INET6, &(sa->sin6_addr), str, sizeof(str));
      LOG_INFO("Address resolved to IPv6 \"%s\", and port \"%s\"\n", str, sock_port_value);
      break;
    }
  }

  if (!ptr) {
    LOG_ERROR("Opening connection failed. Could not resolve either ipv4, or ipv6 address for %s. Error: %d, Message: %s\n", sock_host_value, WSAGetLastError(), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }

  client_sock = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);


  if (client_sock == INVALID_SOCKET) {
    LOG_ERROR("Could not open connection, socket creation failed. Error: %d, Message: %s\n", WSAGetLastError(), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }


  recv_timeout = 30 * 1000;
  setsockopt(client_sock, SOL_SOCKET, SO_RCVTIMEO, (char*) recv_timeout, sizeof(DWORD));

  send_timeout = 30 * 1000;
  setsockopt(client_sock, SOL_SOCKET, SO_SNDTIMEO, (char*) send_timeout, sizeof(DWORD));


  connectResult = connect(client_sock, ptr->ai_addr, ptr->ai_addrlen);
  if (connectResult == SOCKET_ERROR) {
    LOG_ERROR("Could not open connection, socket connection failed. Error: %d, Message: %s\n", WSAGetLastError(), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }

  freeaddrinfo(result);
  LOG_INFO("Connected to %s:%s\n", sock_host_value, sock_port_value);

  BENCHMARK_END("open_socket");

  return & client_sock;

ERROR_EXIT:
  if (msg) {
    copy_error_message(output, outputSize, msg);
    LocalFree(msg);
  }

  WSACleanup();

  return (SOCKET *) INVALID_SOCKET;
}


void close_socket(SOCKET * sock) {
  LOG_INFO("Closing socket connection.\n");

  char * msg = NULL;
  if (closesocket(*sock) == SOCKET_ERROR) {
    LOG_WARNING("Could not close socket. Error: %d, Message: %s\n", WSAGetLastError(), (msg = WSAGetLastErrorMsg()));
  }

  if (msg)
    LocalFree(msg);

  *sock = INVALID_SOCKET;
}

int read_multiple_chunks(SOCKET * sock, int data_sz, char * output, int outputSize) {
  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;

  if (data_sz > outputSize) {
    LOG_DEBUG("Server sent a packet size(%i), larger than the callExtension buffer size(%i)\n",  data_sz, outputSize);
  }

  char * msg = NULL;
  int err = 0;
  vector<char*> * chunk_list = new vector<char*>();

  int effectiveOutputSize = outputSize - 1;
  int remaining_sz = data_sz;

  LOG_DEBUG("read_multiple_chunks: remaining_sz=%i\n", remaining_sz);
  while(remaining_sz > 0) {
    int chunk_sz = MIN(effectiveOutputSize, remaining_sz);
    LOG_DEBUG("read_multiple_chunks: chunk_sz=%i\n", chunk_sz);
    char * chunk =  new char[((chunk_sz + 1)*sizeof(char))]; // +1 for NULL terminator
    chunk_list->push_back(chunk);

    int data_read_bytes = 0;
    int recv_bytes = 0;
    while (data_read_bytes < chunk_sz &&
      (recv_bytes = recv(*sock, chunk + data_read_bytes, MIN((chunk_sz - data_read_bytes), effectiveOutputSize), 0))) {
        data_read_bytes += recv_bytes;
    }

    if (recv_bytes == SOCKET_ERROR) {
      LOG_ERROR("Error reading one of multi-chunk from socket. Error: %d, Message: %s\n", (err = WSAGetLastError()), (msg = WSAGetLastErrorMsg()));
      goto ERROR_EXIT;
    }

    //fill remaining with ZERO
    int i = data_read_bytes;
    while (i < (chunk_sz + 1))
      chunk[i++] = (char) NULL;

    remaining_sz = remaining_sz - data_read_bytes;
    LOG_DEBUG("read_multiple_chunks: data_read_bytes=%i, chunk=%s\n", data_read_bytes, chunk);
    LOG_DEBUG("read_multiple_chunks: remaining_sz=%i\n", remaining_sz);

  }

  output[0] = (char) NULL;
  char num_buf[32];
  strcat(output, "[");
  for (vector<char*>::iterator it = chunk_list->begin(); it != chunk_list->end(); it++) {
    char * chunk = *it;
    sprintf(num_buf, "\"%#x\"",  (unsigned int) chunk);
    strcat(output, num_buf);
    if (it != (chunk_list->end() - 1)) {
      strcat(output, ",");
    }

    sprintf(num_buf, "%#x",  (unsigned int) chunk);
    chunk_info * info = new chunk_info(num_buf, chunk);
    chunk_map.insert(pair<string, chunk_info*>(*info->key, info));
  }

  strcat(output, "]");
  LOG_DEBUG("read_multiple_chunks: output=%s\n", output);

  BENCHMARK_END("read_multiple_chunks");

  return 0;

ERROR_EXIT:

  if (msg) {
    copy_error_message(output, outputSize, msg);
    LocalFree(msg);
  }

  for (vector<char*>::iterator it = chunk_list->begin(); it != chunk_list->end(); it++) {
    delete *it;
  }

  return err;
}

int read_header(SOCKET * sock, char * output, int outputSize, int * data_sz) {
  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;

  char * msg = NULL;
  int err = 0;

  int header_sz = 4;
  char * header = new char[header_sz]; // +1 for NULL terminator

  int data_read_bytes = 0;
  int recv_bytes = 0;
  while (data_read_bytes < header_sz &&
    (recv_bytes = recv(*sock, header + data_read_bytes, (header_sz - data_read_bytes), 0))) {
      data_read_bytes += recv_bytes;
  }

  if (recv_bytes == SOCKET_ERROR) {
    LOG_ERROR("read_size: Error reading header socket. Error: %d, Message: %s\n", (err = WSAGetLastError()), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }

  *data_sz = (int) ntohl(* (u_long *) (void *) header);

  LOG_DEBUG("read_size: data_sz=%i\n", *data_sz);

  if (header)
    delete header;

  BENCHMARK_END("read_size");
  return 0;

ERROR_EXIT:
  if (msg) {
    copy_error_message(output, outputSize, msg);
    LocalFree(msg);
  }

  if (header)
    delete header;

  return err;
}

int read_data(SOCKET * sock, char * output, int outputSize) {
  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;

  int err = 0;

  int data_sz = 0;

  if ((err = read_header(sock, output, outputSize, &data_sz))) {
    goto ERROR_EXIT;
  }

  if ((err = read_multiple_chunks(sock, data_sz, output, outputSize))) {
    goto ERROR_EXIT;
  }


  BENCHMARK_END("read_data");

  return 0;

ERROR_EXIT:

  return err;
}

int send_data(SOCKET * sock, const char * data, char * output, int outputSize) {
  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;

  int data_sz = strlen(data);

  u_long ul_data_sz = htonl((u_long) data_sz);

  int sent_sz = 0;
  char * msg = NULL;
  int err = 0;

  //Send the packet size
  LOG_DEBUG("send_data: size = %d\n", data_sz);
  WSASetLastError(0);
  sent_sz = send(*sock, (char*) & ul_data_sz,  sizeof(u_long), 0);
  if (sent_sz == SOCKET_ERROR) {
    LOG_ERROR("Error sending packet size on socket. Error: %d, Message: %s\n", (err = WSAGetLastError()), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }

  //Send the actual data
  LOG_DEBUG("send_data: data = %s\n", data);
  WSASetLastError(0);
  sent_sz = send(*sock, data, data_sz, 0);
  if (sent_sz == SOCKET_ERROR) {
    LOG_ERROR("Error sending data on socket. Error: %d, Message: %s\n", (err = WSAGetLastError()), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }

  BENCHMARK_END("send_data");

  return 0;

ERROR_EXIT:
  if (msg) {
    copy_error_message(output, outputSize, msg);
    LocalFree(msg);
  }

  return err;
}


#ifdef __linux
class LibTracker {
 public:
  LibTracker() {
     read_parameters();
   }
   ~LibTracker() {
     close_log();
   }
 };
static LibTracker tracker;
#else
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
  switch (ul_reason_for_call) {
    case DLL_PROCESS_DETACH:
      close_log();
      break;
    case DLL_PROCESS_ATTACH:
      read_parameters();
      break;
    case DLL_THREAD_ATTACH:
      break;
    case DLL_THREAD_DETACH:
      break;
  }
  return TRUE;
}
#endif

#ifndef __linux
extern "C" {  __declspec(dllexport) void __stdcall RVExtension(char *output, int outputSize, const char *function); };


void ignore(const wchar_t* expression, const wchar_t* function, const wchar_t* file, unsigned int line, uintptr_t pReserved) {
  return;
}
#endif

int set_keep_alive(SOCKET * sock, char * output, int outputSize) {
  char * msg = NULL;
  int err = 0;
  BOOL optVal = TRUE;

  int result = setsockopt(*sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &optVal, sizeof(BOOL));
  if (result == SOCKET_ERROR) {
    LOG_ERROR("Error setting SO_KEEPALIVE on socket. Error: %d, Message: %s\n", (err = WSAGetLastError()), (msg = WSAGetLastErrorMsg()));
    goto ERROR_EXIT;
  }

ERROR_EXIT:
    if (msg) {
      copy_error_message(output, outputSize, msg);
      LocalFree(msg);
    }
    return err;
}

void handle_connection(char * output, int outputSize, const char * entry) {
  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;


  SOCKET * sock;
  if ((sock = open_socket(output, outputSize)) == (SOCKET *) INVALID_SOCKET) {
    return;
  }


  //try to re-open the socket if keep alive fails
  if (set_keep_alive(sock, output, outputSize)) {
    LOG_INFO("Setting KEEP_ALIVE failed. Attempting to close, and re-open socket\n");
    close_socket(sock);
    if ((sock = open_socket(NULL, (int) NULL)) == (SOCKET *) INVALID_SOCKET) {
      return;
    }
  }

  //try sending an empty packet first, to check if connection is open
  if (send_data(sock, "", output, outputSize)) {
    LOG_INFO("Sending zero-length packet failed. Attempting to close, and re-open socket\n");
    close_socket(sock);
    if ((sock = open_socket(NULL, (int) NULL)) == (SOCKET *) INVALID_SOCKET) {
      return;
    }
  }

  if (send_data(sock, entry, output, outputSize)) {
    close_socket(sock);
    return;
  }

  if (read_data(sock, output, outputSize)) {
    close_socket(sock);
    return;
  }

  BENCHMARK_END("handle_connection");

}

void handle_chunk(char * output, int outputSize, const char * entry) {
  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;

  string chunk_addr_str(entry);
  LOG_DEBUG("handle_chunk: entry=%s\n", chunk_addr_str.c_str());


  map<string, chunk_info*>::iterator it = chunk_map.find(chunk_addr_str);
  if (it == chunk_map.end()) {
    LOG_DEBUG("handle_chunk: could not locate chunk\n");
    return;
  }

  chunk_info * info = it->second;
  char * location = info->location;

  u_int effectiveOutputSize = outputSize - 1; //-1 for NULL terminator
  int result_sz = MIN(strlen(location), effectiveOutputSize);
  strncpy(output, location, result_sz);
  int i = result_sz;
  while (i < outputSize)
    output[i++] = (char) NULL;

  LOG_DEBUG("handle_chunk: output=%s\n", output);

  chunk_map.erase(it);
  delete info;

  BENCHMARK_END("handle_chunk");
}

//no need to synchronize the cleanup, as SQF is single threaded
void cleanup_chunks() {
  BENCHMARK_SETUP;
  BENCHMARK_BEGIN;

  time_t current_time;
  time(&current_time);


  map<string, chunk_info*>::iterator it = chunk_map.begin();
  while( it != chunk_map.end()) {
    chunk_info * info = it->second;
    double lifetime = difftime(current_time, info->created_time);

    if (lifetime > sock_chunk_lifetime_value) {
      LOG_DEBUG("cleanup_chunks: chunk %s, lifetime=%f, sock_chunk_lifetime_value=%u, deleted\n", info->key->c_str(), lifetime, sock_chunk_lifetime_value);

#ifdef __linux
      chunk_map.erase(it++);
#else
      it = chunk_map.erase(it);
#endif
      delete info;
      continue;
    }

    it++;
  }

  BENCHMARK_END("cleanup_chunks");
}



#ifdef __linux
extern "C" void RVExtension(char *output, int outputSize, const char *entry) {
#else
  void __stdcall RVExtension(char *output, int outputSize, const char * entry) {
    _invalid_parameter_handler handler = _set_invalid_parameter_handler(ignore);
    _CrtSetReportMode(_CRT_ASSERT, 0);

#endif
    char * chunk_address;

    if (!strcmp(entry, "version")) {
      strncpy(output, version, outputSize);
      goto EXIT;
    }

    cleanup_chunks();

    chunk_address = NULL;
    if (sscanf(entry, "%x", (u_int*) &chunk_address) > 0) {
      handle_chunk(output, outputSize, entry);
      goto EXIT;
    }

    handle_connection(output, outputSize, entry);

EXIT:
#ifndef __linux
    if (handler)
      _set_invalid_parameter_handler(handler);
#endif
    return;
}


