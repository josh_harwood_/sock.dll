SHELL=/bin/bash

all: release

debug: CC = g++ -g
debug: DIR = Debug
debug: setup sock.so

release: CC = g++ -O3 
release: DIR = Release
release: setup sock.so

setup:
	test -d ${DIR} || mkdir ${DIR}

sock.so: clean main.o
	@echo 'Building target: $@'
	@echo 'Invoking: Cross G++ Linker'
	${CC} -m32 -shared -o "${DIR}/sock.so"  ${DIR}/main.o
	@echo 'Finished building target: $@'

main.o:
	@echo 'Building target: $@'
	@echo 'Invoking: Cross G++ Compiler'
	${CC} -Wall -c -fmessage-length=0 -m32 -fPIC -o "${DIR}/main.o" "src/Main.cpp"
	@echo 'Finished building target: $@'

clean: 
	rm -f {Release,Debug}/*.{o,so}
